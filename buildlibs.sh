#!/bin/sh

export MAJOR=opt
export MINOR=full

export PREFIX=/home/ali/SEClang-${MAJOR}/stdlib-${MINOR}
export CC=/home/ali/SEClang-${MAJOR}/clang-${MINOR}
export CXX=/home/ali/SEClang-${MAJOR}/clang++-${MINOR}

cd libc
make clean
make -j 16 LIBCDIR=/usr/src/lib/libc
cp libc* $PREFIX/
cd ..

cd libm
make clean
make -j 16 LIBCDIR=/usr/src/lib/libc
cp libm* $PREFIX/
cd ..

cd libthr
make clean
make -j 16 LIBCDIR=/usr/src/lib/libc
cp libthr* $PREFIX/
cd ..

cd libcxxrt
make clean
make -j 16 LIBCDIR=/usr/src/lib/libc
cp libcxxrt* $PREFIX/
cd ..

cd libc++
make clean
make -j 16 LIBCDIR=/usr/src/lib/libc
cp libc++* $PREFIX/
cd ..

